# Flower Network Flow Analysis

These Dockerfiles allow you to quickly set up a fully operational Flower server. Docker commands in the scripts described below are customized for a Triton SmartOS installation and may need to be tweaked for your purposes.

1. Start with the `flower-base` image. Moving in to that directory and issuing a "./build" should be sufficient.
2. Next, build the `flower-app` image by entering that directory and issuing "./deploy".

At this point, your server should be running. To begin using it, you'll need to create an initial organization and account. You'll also need to start the `flower-connector` (that can't be started until you have an organization defined).

Launch a shell in the running container: `docker exec -it <CONTAINER_UUID> /bin/bash`

From the container, create an organization with:

~~~~
UUID=`curl -X POST http://localhost:8080/organization/create -H "Content-type:application/json" -d "{\"name\":\"org\"}"`
~~~~

Also from the container, create an account with:

~~~~
curl -X POST http://localhost:8080/account/create -H "Content-type:application/json" -d "{\"username\":\"<ACCOUNTNAME>\", \"password\":\"<INITIALPASSWORD>\", \"organization\":\"$UUID\"}"
~~~~

Now, start the connector service:

~~~~
cd /srv/flower-connector
./connector.py -ndSM localhost $UUID
~~~~

Log in from your browser with your username and password to `http://<ADDRESS>:4567` and start sending Netflow v9 events to UDP port 9995 on the same address. You'll need to define at least one network to map in the browser interface in order for connections to start appearing.
